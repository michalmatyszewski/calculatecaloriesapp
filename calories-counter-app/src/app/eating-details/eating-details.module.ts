import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EatingDetailsPageRoutingModule } from './eating-details-routing.module';

import { EatingDetailsPage } from './eating-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EatingDetailsPageRoutingModule
  ],
  declarations: [EatingDetailsPage]
})
export class EatingDetailsPageModule {}
