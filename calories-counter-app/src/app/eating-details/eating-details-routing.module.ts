import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EatingDetailsPage } from './eating-details.page';

const routes: Routes = [
  {
    path: '',
    component: EatingDetailsPage
  },
  {
    path: 'sniadanie',
    redirectTo: '../tab1/eating-times-breakfast'
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EatingDetailsPageRoutingModule {}
