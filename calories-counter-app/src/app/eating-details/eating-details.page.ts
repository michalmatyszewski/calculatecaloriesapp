import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-eating-details',
  templateUrl: './eating-details.page.html',
  styleUrls: ['./eating-details.page.scss'],
})
export class EatingDetailsPage implements OnInit {

  name: any;

  constructor(private activeRoutes: ActivatedRoute, private router: Router) { 
    this.name = this.activeRoutes.snapshot.paramMap.get('name');
  }

  ngOnInit() {
  }

  back(){
    this.router.navigateByUrl('../tab1/eating-times-breakfast');
  }

}
