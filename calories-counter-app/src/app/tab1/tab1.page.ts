import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { EatingDetailsPage } from '../eating-details/eating-details.page';
import { ApiService } from '../shared/service/api.service';
import { EatingTimesPage } from './eating-times/eating-times.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  allCalories : number = 0;
  kcl : number = 0;
  roznica_kcal = 0;
  procentowo = 0;

  constructor(public router: Router, private api : ApiService) {}

  ionViewDidEnter(){
    this.allCalories = 0;
    this.getBreakfast();
    this.getLunch();
    this.getObiad();
    this.getKolacja();
    this.getUser();
  }



  goToFood(name: string){
    this.router.navigate(['/tabs/tab1/eating-times-day', {name}]);
}


getBreakfast() {
  this.api.getBreakfast()
    .subscribe({
      next: (res) => {
        for (let item of res){
          this.allCalories = this.allCalories + item.kcal;
        }
        console.log(this.allCalories);
      },
      error: (err) => {
        alert("Error while featching the Records")
      }
    })
}

getLunch(){
  this.api.getLunch()
  .subscribe({
    next: (res) => {
      for (let item of res){
        this.allCalories = this.allCalories + item.kcal;
      }
      console.log(this.allCalories);
    },
    error: (err) => {
      alert("Error while featching the Records")
    }
  })
}

getObiad(){
  this.api.getObiad()
  .subscribe({
    next: (res) => {
      for (let item of res){
        this.allCalories = this.allCalories + item.kcal;
      }
      console.log(this.allCalories);
    },
    error: (err) => {
      alert("Error while featching the Records")
    }
  })
}

getKolacja(){
  this.api.getKolacja()
  .subscribe({
    next: (res) => {
      for (let item of res){
        this.allCalories = this.allCalories + item.kcal;
      }
      console.log(this.allCalories);
    },
    error: (err) => {
      alert("Error while featching the Records")
    }
  })
}

getUser(){
  this.api.getUser()
  .subscribe({
    next: (res) => {
      this.kcl = res.dawkaKcal;
      this.roznica_kcal = this.kcl - this.allCalories;
      this.procentowo = Math.round((this.allCalories/this.kcl) *100);
    },
    error: (err) => {
      alert("Error while featching the Records")
    }
  })
}

}
