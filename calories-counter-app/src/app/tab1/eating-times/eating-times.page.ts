import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DishModel } from 'src/app/shared/interface/dish.model';
import { FoodModel } from 'src/app/shared/interface/food.model';
import { ProductModel } from 'src/app/shared/interface/product.model';
import { ApiService } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-eating-times',
  templateUrl: './eating-times.page.html',
  styleUrls: ['./eating-times.page.scss'],
})

export class EatingTimesPage implements OnInit {

  id;
  breakfast !: any;
  name: any;
  products : ProductModel;
  dishes : DishModel;
  item : FoodModel = new FoodModel;


  constructor(private router: Router, private api: ApiService, private activeRoutes: ActivatedRoute) {
    this.name = this.activeRoutes.snapshot.paramMap.get('name');
    console.log(this.name);
   }

   ionViewDidEnter(){
    this.getProduct();
    this.getDishes();
  }

  ngOnInit() {}

  back(name: string){
    this.router.navigate(['/tabs/tab1/eating-times-day', {name}]);
  }

  getProduct() {
    this.api.getProduct()
      .subscribe({
        next: (res) => {
          this.products = res;
          console.log(this.products);
        },
        error: (err) => {
          alert("Error while featching the Records")
        }
      })
  }
  getDishes() {
    this.api.getDish()
      .subscribe({
        next: (res) => {
          this.dishes = res;
          console.log(this.dishes);
        },
        error: (err) => {
          alert("Error while featching the Records")
        }
      })
  }

  post(name_dish, kcal, description){
    this.item.name = name_dish;
    this.item.kcal = kcal;
    this.item.description = description;

    if (this.name == "Sniadanie"){
      this.postBreakfast(this.item);
    }else if(this.name == "Lunch"){
      this.postLunch(this.item);
    }else if(this.name == "Obiad"){
      this.postObiad(this.item);
    }else if(this.name == "Kolacja"){
      this.postKolacja(this.item);
    }
  }

  postBreakfast(item: any){
    this.api.postBreakfast(item)
    .subscribe(res=>{
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }

  postLunch(item: any){
    this.api.postLunch(item)
    .subscribe(res=>{
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }

  postObiad(item: any){
    this.api.postObiad(item)
    .subscribe(res=>{
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }

  postKolacja(item: any){
    this.api.postKolacja(item)
    .subscribe(res=>{
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }



  
  



}
