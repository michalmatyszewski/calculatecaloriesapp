import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EatingTimesPageRoutingModule } from './eating-times-routing.module';

import { EatingTimesPage } from './eating-times.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: EatingTimesPage
  },
  {
    path: ':id',
    component: EatingTimesPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EatingTimesPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EatingTimesPage]
})
export class EatingTimesPageModule {}
