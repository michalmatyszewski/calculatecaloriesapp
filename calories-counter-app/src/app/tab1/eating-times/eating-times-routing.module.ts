import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EatingTimesPage } from './eating-times.page';

const routes: Routes = [
  {
    path: '',
    component: EatingTimesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EatingTimesPageRoutingModule {}
