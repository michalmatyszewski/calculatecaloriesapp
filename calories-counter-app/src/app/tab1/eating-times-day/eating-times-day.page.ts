import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakfastModel } from 'src/app/shared/interface/breakfast.model';
import { ApiService } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-eating-times-day',
  templateUrl: './eating-times-day.page.html',
  styleUrls: ['./eating-times-day.page.scss'],
})
export class EatingTimesDayPage implements OnInit {

  food: BreakfastModel[] =[];
  allCalories: number = 0;
  name : string;

  constructor(private api: ApiService, private router: Router, private activeRoutes : ActivatedRoute) {
    this.name = this.activeRoutes.snapshot.paramMap.get('name');
   }
   
   ionViewDidEnter(){
    if(this.name=='Sniadanie'){
      this.getBreakfast();
    }else if(this.name=='Lunch'){
      this.getLunch();
    }else if(this.name=='Obiad'){
      this.getObiad();
    }else if(this.name=='Kolacja'){
      this.getKolacja();
    }
  }
  ngOnInit() {
  }

  goToAddNewItem(name: string){
      this.router.navigate(['/tabs/tab1/eating-times', {name}]);
  }

  removeItem( id : number ){
    console.log(id);
    this.deleteBreakfast(id);
    
  }


  getBreakfast() {
    this.api.getBreakfast()
      .subscribe({
        next: (res) => {
          this.food = res;
          for (let item of res){
            this.allCalories = this.allCalories + item.kcal;
          }
          console.log(this.food);
        },
        error: (err) => {
          alert("Error while featching the Records")
        }
      })
  }

  deleteBreakfast(id: number) {
    this.api.deleteBreakfast(id)
    .subscribe(res=>{
      this.getBreakfast();
    },
    err=>{
      alert("Something wrong")
    })
    
  }

  getLunch(){
    this.api.getLunch()
    .subscribe({
      next: (res) => {
        this.food = res;
        for (let item of res){
          this.allCalories = this.allCalories + item.kcal;
        }
        console.log(this.allCalories);
      },
      error: (err) => {
        alert("Error while featching the Records")
      }
    })
  }

  getObiad(){
    this.api.getObiad()
    .subscribe({
      next: (res) => {
        this.food = res;
        for (let item of res){
          this.allCalories = this.allCalories + item.kcal;
        }
        console.log(this.allCalories);
      },
      error: (err) => {
        alert("Error while featching the Records")
      }
    })
  }

  getKolacja(){
    this.api.getKolacja()
    .subscribe({
      next: (res) => {
        this.food = res;
        for (let item of res){
          this.allCalories = this.allCalories + item.kcal;
        }
        console.log(this.allCalories);
      },
      error: (err) => {
        alert("Error while featching the Records")
      }
    })
  }

}
