import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EatingTimesDayPage } from './eating-times-day.page';

const routes: Routes = [
  {
    path: '',
    component: EatingTimesDayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EatingTimesDayPageRoutingModule {}
