import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab1Page } from './tab1.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'eating-times',
    loadChildren: () => import('./eating-times/eating-times.module').then( m => m.EatingTimesPageModule)
  },
  {
    path: 'eating-times-day',
    loadChildren: () => import('./eating-times-day/eating-times-day.module').then( m => m.EatingTimesDayPageModule)
  }

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
