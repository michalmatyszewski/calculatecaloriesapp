import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ProductModel } from 'src/app/shared/interface/product.model';
import { ApiService } from 'src/app/shared/service/api.service';
import { DishModel } from '../shared/interface/dish.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {


  productModelObj : ProductModel = new ProductModel();
  dishModelObj : DishModel = new DishModel();

  formValue: FormGroup
  formValue1: FormGroup

  constructor(private formbuilder: FormBuilder,
    private api : ApiService) {}
  
  ngOnInit(): void {
    this.formValue = new FormGroup({
      name: new FormControl(),
      kcal: new FormControl()
    });

    this.formValue1 = new FormGroup({
      name: new FormControl(),
      kcal: new FormControl(),
      descripton: new FormControl()
    });
  }

  postProductDetails(){
    this.productModelObj.name = this.formValue.value.name;
    this.productModelObj.kcal = Number(this.formValue.value.kcal);

    this.api.postProduct(this.productModelObj)
    .subscribe(res=>{
      console.log(res);
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }

  postDishDetails(){
    this.dishModelObj.name = this.formValue1.value.name;
    this.dishModelObj.kcal = Number(this.formValue1.value.kcal);
    this.dishModelObj.descripton = this.formValue1.value.descripton

    this.api.postDish(this.dishModelObj)
    .subscribe(res=>{
      console.log(res);
      alert("Product Added")
    },
    err=>{
      alert("Something wrong")
    })
  }
}