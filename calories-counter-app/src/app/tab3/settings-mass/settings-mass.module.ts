import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsMassPageRoutingModule } from './settings-mass-routing.module';

import { SettingsMassPage } from './settings-mass.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsMassPageRoutingModule
  ],
  declarations: [SettingsMassPage]
})
export class SettingsMassPageModule {}
