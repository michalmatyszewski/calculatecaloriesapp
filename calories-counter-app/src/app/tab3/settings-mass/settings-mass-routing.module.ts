import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsMassPage } from './settings-mass.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsMassPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsMassPageRoutingModule {}
