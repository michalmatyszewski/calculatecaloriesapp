import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab3Page } from './tab3.page';

const routes: Routes = [
  {
    path: '',
    component: Tab3Page,
  },  {
    path: 'settings-name',
    loadChildren: () => import('./settings-name/settings-name.module').then( m => m.SettingsNamePageModule)
  },
  {
    path: 'settings-age',
    loadChildren: () => import('./settings-age/settings-age.module').then( m => m.SettingsAgePageModule)
  },
  {
    path: 'settings-kcal',
    loadChildren: () => import('./settings-kcal/settings-kcal.module').then( m => m.SettingsKcalPageModule)
  },
  {
    path: 'settings-mass',
    loadChildren: () => import('./settings-mass/settings-mass.module').then( m => m.SettingsMassPageModule)
  },
  {
    path: 'settings-height',
    loadChildren: () => import('./settings-height/settings-height.module').then( m => m.SettingsHeightPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
