import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsKcalPage } from './settings-kcal.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsKcalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsKcalPageRoutingModule {}
