import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsKcalPageRoutingModule } from './settings-kcal-routing.module';

import { SettingsKcalPage } from './settings-kcal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsKcalPageRoutingModule
  ],
  declarations: [SettingsKcalPage]
})
export class SettingsKcalPageModule {}
