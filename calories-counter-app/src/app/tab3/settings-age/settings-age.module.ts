import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsAgePageRoutingModule } from './settings-age-routing.module';

import { SettingsAgePage } from './settings-age.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsAgePageRoutingModule
  ],
  declarations: [SettingsAgePage]
})
export class SettingsAgePageModule {}
