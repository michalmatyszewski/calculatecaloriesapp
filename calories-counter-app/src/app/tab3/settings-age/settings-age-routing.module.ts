import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsAgePage } from './settings-age.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsAgePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsAgePageRoutingModule {}
