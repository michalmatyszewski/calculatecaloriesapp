import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/shared/interface/user.model';
import { ApiService } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  formValue !: FormGroup;
  newData : any;
  id : number = 1;
  type: string = '';
  user: User = new User;


  constructor(private activeRoutes : ActivatedRoute, private api: ApiService, private formbuilder: FormBuilder) {
    this.type = this.activeRoutes.snapshot.paramMap.get('type');
    this.newData = this.activeRoutes.snapshot.paramMap.get('data');
  }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      firstName : ['']
    })
    this.getUser();
  }
  updateUser(){

    if(this.type=='imie'){
      this.user.imie = this.newData;
    }else if(this.type=="wiek"){
      this.user.wiek = Number(this.newData);
    }else if(this.type=="wzrost"){
      this.user.wzrost = Number(this.newData);
    }else if(this.type=="waga"){
      this.user.waga = Number(this.newData);
    }else if(this.type=="dawkaKcal"){
      this.user.dawkaKcal = Number(this.newData);
    }

    this.api.updateUser(this.user, 1)
    .subscribe(res=>{
      alert("Updated Succesfully")
    })

    // this.api.updateKolacja(this.kolacja, this.id)
    // .subscribe(res=>{
    //     alert("Updated Succesfully")
    // })

    console.log(this.user);
  }

  getUser() {
    this.api.getUser()
      .subscribe({
        next: (res) => {
          this.user = res;
          console.log(this.user);
        },
        error: (err) => {
          alert("Error while featching the Records")
        }
      })
      
  }

}
