import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../shared/service/api.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  user !: any;
  constructor(private api: ApiService, private router: Router) {
   
  }

  ionViewDidEnter(){
    this.getUser();
  }

  ngOnInit(): void {
    
  }


  getUser() {
    this.api.getUser()
      .subscribe({
        next: (res) => {
          this.user = res;
        },
        error: (err) => {
          alert("Error while featching the Records")
        }
      })
  }

  goToSettings(type, data){
    this.router.navigate(['/tabs/tab3/settings', {type, data}]);
  }

}
