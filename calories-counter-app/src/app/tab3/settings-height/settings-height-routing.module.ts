import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsHeightPage } from './settings-height.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsHeightPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsHeightPageRoutingModule {}
