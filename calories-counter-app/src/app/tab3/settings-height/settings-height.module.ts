import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsHeightPageRoutingModule } from './settings-height-routing.module';

import { SettingsHeightPage } from './settings-height.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsHeightPageRoutingModule
  ],
  declarations: [SettingsHeightPage]
})
export class SettingsHeightPageModule {}
