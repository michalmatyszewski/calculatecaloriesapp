import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsNamePageRoutingModule } from './settings-name-routing.module';

import { SettingsNamePage } from './settings-name.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsNamePageRoutingModule
  ],
  declarations: [SettingsNamePage]
})
export class SettingsNamePageModule {}
