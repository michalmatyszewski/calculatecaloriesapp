import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsNamePage } from './settings-name.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsNamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsNamePageRoutingModule {}
