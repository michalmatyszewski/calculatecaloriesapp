import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }

  getBreakfast(){
    return this.http.get<any>("http://localhost:3000/Sniadanie")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postBreakfast(data: any) {
    return this.http.post<any>("http://localhost:3000/Sniadanie", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  deleteBreakfast(id : number){
    return this.http.delete<any>("http://localhost:3000/Sniadanie/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getLunch(){
    return this.http.get<any>("http://localhost:3000/Lunch")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postLunch(data: any) {
    return this.http.post<any>("http://localhost:3000/Lunch", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getObiad(){
    return this.http.get<any>("http://localhost:3000/Obiad")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postObiad(data: any) {
    return this.http.post<any>("http://localhost:3000/Obiad", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getKolacja(){
    return this.http.get<any>("http://localhost:3000/Kolacja")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postKolacja(data: any) {
    return this.http.post<any>("http://localhost:3000/Kolacja", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postProduct(data : any){
    return this.http.post<any>("http://localhost:3000/NowyProdukt", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getProduct(){
    return this.http.get<any>("http://localhost:3000/NowyProdukt")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  postDish(data : any){
    return this.http.post<any>("http://localhost:3000/NoweDanie", data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getDish(){
    return this.http.get<any>("http://localhost:3000/NoweDanie")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  getUser(){
    return this.http.get<any>("http://localhost:3000/Uzytkownik")
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  updateUser(data, id){
    return this.http.put<any>("http://localhost:3000/Uzytkownik", data)
    .pipe(map((res:any)=>{
      return res
    }))
  }
  
}
