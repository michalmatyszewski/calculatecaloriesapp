export class ProductModel {
    id : number = 0;
    name : string = '';
    kcal : number = 0;
    description : string = '';
}