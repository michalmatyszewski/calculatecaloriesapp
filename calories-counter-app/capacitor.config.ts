import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'calories-counter-app',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
